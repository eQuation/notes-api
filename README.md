# Notes API

Demo RESTful API.

## Getting Started

### Prerequisites

The following must be installed:

* [Java 8](http://openjdk.java.net/install/)
* [maven](http://maven.apache.org/install.html)

### Installing

1. Navigate to the project's root directory.

1. Execute the command `mvn clean install`. This will download all of the dependencies you need and run the tests in
the application. At the end it should say `BUILD SUCCESS`.

### Running

1. Navigate to the project's root directory.

1. Execute the command `mvn spring-boot:run`. The API should now be available for you at `localhost:8080`.

## About this Project

This project is meant to showcase my familiarity with creating RESTful APIs.
It was created with pretty simple requirements, namely C R ~U~ ~D~ on a `Note` object.

### Tools used

#### Plaster

The first tool I used was `plaster`, a Jacob Drost product. He and I have both created many CRUD apps,
and he decided to script the whole process. You can find out more about that tool
[here](https://github.com/JDrost1818/plaster), but long story short I generated much of this code
with this command:

`plaster g scaffold Note body:string`

The project has been lacking in upkeep for a bit so there was a decent amount of deprecations/compilation errors,
but it writes a lot of the basics for you. There are other tools out there that accomplish a similar goal,
but this is the best one I know of.

#### SpringBoot Web & Test & JPA

The SpringBoot framework has been invaluable to me in all my Java projects. I won't go into too much detail, but you can find more
information [here](https://spring.io/guides/gs/spring-boot/).

#### H2 Database

Since this is just a demo project, I didn't think it was worth spending the time setting up an actual datasource. H2 is
a popular in-memory datasource that integrates really well with Spring, so I chose that. Find out more 
[here](http://www.h2database.com/html/main.html).

#### Hibernate JPA 2 Metamodel Generator

I use this JBoss tool to generate meta-models that increase type safety when working with the Spring JPA Specification
framework. Find out more [here](https://docs.jboss.org/hibernate/jpamodelgen/1.0/reference/en-US/html_single/).

#### Lombok

Lombok is my favorite utility dependency in Java. It provides the `@Data` and `@Builder` annotations as examples. Putting
just those annotations on a model will automatically make all of the getters/setters, builders, toStrings, and hashCode
functions you need on that model. It's very slick.


## Author

* **John Mollberg** - *Sole author*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
