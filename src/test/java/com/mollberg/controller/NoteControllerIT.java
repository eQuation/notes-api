package com.mollberg.controller;

import com.mollberg.repository.NoteRepository;
import com.mollberg.domain.model.Note;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class NoteControllerIT {

    @Autowired
    private NoteRepository repo;

    @Autowired
    private NoteController classUnderTest;

    @Test
    public void testCreate() throws Exception {
        Note instance = new Note();
        Integer id = instance.getId();

        Note createdInstance = this.classUnderTest.create(instance);

        assertThat(id, not(createdInstance.getId()));
    }

    @Test
    public void testRead() throws Exception {
        Note instanceToFind = this.saveInstance();
        Integer id = instanceToFind.getId();

        Note foundInstance = this.classUnderTest.read(id);

        assertThat(instanceToFind, equalTo(foundInstance));
    }

    @Test
    public void testDelete() throws Exception {
        Note instanceToFind = this.saveInstance();
        Integer id = instanceToFind.getId();

        assertThat(this.classUnderTest.read(id), is(notNullValue()));

        this.classUnderTest.delete(id);

        assertThat(this.classUnderTest.read(id), is(nullValue()));
    }

    public Note saveInstance() {
        return this.repo.save(new Note());
    }

}