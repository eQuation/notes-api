package com.mollberg.controller;

import com.mollberg.service.NoteService;
import com.mollberg.domain.model.Note;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class NoteControllerTest {

    @Mock
    private NoteService noteService;

    @InjectMocks
    private NoteController classUnderTest;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
    }

    @Test
    public void testCreate() {
        Note instance = new Note();
        when(this.noteService.create(same(instance))).thenReturn(instance);

        assertThat(instance, sameInstance(this.classUnderTest.create(instance)));
    }

    @Test
    public void testRead() {
        Integer id = 1;
        Note instance = new Note();

        when(this.noteService.read(id)).thenReturn(instance);

        assertThat(instance, sameInstance(this.classUnderTest.read(id)));
    }

    @Test
    public void testUpdate() {
        Integer id = 1;
        Note instance = new Note();

        when(this.noteService.update(same(instance))).thenReturn(instance);

        assertThat(instance, sameInstance(this.classUnderTest.update(id, instance)));
    }

    @Test
    public void testDelete() {
        Integer id = 1;

        this.classUnderTest.delete(id);

        verify(this.noteService, times(1)).delete(id);
    }

}