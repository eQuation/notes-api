package com.mollberg.service;

import com.mollberg.repository.NoteRepository;
import com.mollberg.domain.model.Note;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class NoteServiceTest {

    @Mock
    private NoteRepository noteRepository;

    @InjectMocks
    private NoteService classUnderTest;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
    }

    @Test
    public void testCreate() {
        Note instance = new Note();
        when(this.noteRepository.save(same(instance))).thenReturn(instance);

        assertThat(instance, sameInstance(this.classUnderTest.create(instance)));
    }

    @Test
    public void testRead() {
        Integer id = 1;
        Note instance = new Note();

        when(this.noteRepository.findById(id)).thenReturn(Optional.of(instance));

        assertThat(instance, sameInstance(this.classUnderTest.read(id)));
    }

    @Test
    public void testUpdate() {
        Note instance = new Note();
        when(this.noteRepository.save(same(instance))).thenReturn(instance);

        assertThat(instance, sameInstance(this.classUnderTest.update(instance)));
    }

    @Test
    public void testDelete() {
        Integer id = 1;

        this.classUnderTest.delete(id);

        verify(this.noteRepository, times(1)).deleteById(id);
    }

}