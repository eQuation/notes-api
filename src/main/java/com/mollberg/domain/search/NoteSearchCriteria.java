package com.mollberg.domain.search;

import com.mollberg.domain.model.Note;
import com.mollberg.domain.model.Note_;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import static org.apache.logging.log4j.util.Strings.isNotEmpty;

/**
 * {@link Specification} for {@link Note}s.
 * <p>
 * This class defines how searches can be performed on {@link Note}s.
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Slf4j
public class NoteSearchCriteria implements Specification<Note> {

    /**
     * Search string for {@link Note}s. The criteria will return {@link Note}s
     * where {@link Note#body} contains this query.
     */
    private String query;

    @Override
    public Predicate toPredicate(Root<Note> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        boolean hasQuery = isNotEmpty(this.query);

        if (hasQuery) {
            return criteriaBuilder.like(
                    criteriaBuilder.upper(root.get(Note_.body)),
                    "%" + this.query.toUpperCase() + "%");
        }

        return null;
    }
}