package com.mollberg.controller;

import com.mollberg.domain.search.NoteSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.mollberg.service.NoteService;
import com.mollberg.domain.model.Note;

import java.util.Collection;

import static com.mollberg.config.ApiConfig.BASE_URL;

@RestController
@RequestMapping(BASE_URL + "/notes")
public class NoteController {

    private final NoteService noteService;

    @Autowired
    public NoteController(NoteService noteService) {
            this.noteService = noteService;
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public Note create(@RequestBody Note note) {
            return this.noteService.create(note);
    }

    @GetMapping("/{id}")
    public Note read(@PathVariable Integer id) {
            return this.noteService.read(id);
    }

    @GetMapping
    public Collection<Note> search(NoteSearchCriteria searchCriteria) {
        return this.noteService.search(searchCriteria);
    }

    @PutMapping("/{id}")
    public Note update(@PathVariable Integer id, @RequestBody Note note) {
            return this.noteService.update(note);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
            this.noteService.delete(id);
    }

}
