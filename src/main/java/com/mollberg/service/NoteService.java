package com.mollberg.service;

import com.mollberg.domain.search.NoteSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.mollberg.domain.model.Note;
import com.mollberg.repository.NoteRepository;

import java.util.Collection;

@Service
public class NoteService {

    private final NoteRepository noteRepository;
    
    @Autowired
    public NoteService(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }
    
    public Note create(Note note) {
        return this.noteRepository.save(note);
    }
    
    public Note read(Integer id) {
        return this.noteRepository.findById(id).orElse(null);
    }
    
    public Note update(Note note) {
        return this.noteRepository.save(note);
    }
    
    public void delete(Integer id) {
        this.noteRepository.deleteById(id);
    }

    public Collection<Note> search(NoteSearchCriteria searchCriteria) {
        return this.noteRepository.findAll(searchCriteria, Pageable.unpaged()).getContent();
    }
}