package com.mollberg.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;

import com.mollberg.domain.model.Note;
import org.springframework.stereotype.Repository;

@Repository
public interface NoteRepository extends CrudRepository<Note, Integer> {

    Page<Note> findAll(Specification<Note> spec, Pageable pageInfo);

    Note findOne(Specification<Note> spec);

}